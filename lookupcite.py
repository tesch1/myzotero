#!/usr/bin/env python
#
# get the data in a panda frame
#

from __future__ import print_function
from crossref.restful import Works,Etiquette,LIMIT
import sys

exec(open("./config.py").read())

if __name__ == "__main__":

    works = Works()
    etiquette = Etiquette(application_name=application_name,
                          application_version=application_version,
                          application_url=application_url,
                          contact_email = contact_email)
    LIMIT = 10
    
    q = works.query(title = sys.argv[1])
    matches = q.count()
    w1 = q.order('desc').sort('relevance')
    print('Url: %s | Number of matches: %d' % (q.url, matches))
    n = 0
    for witem in w1:
        #print('keys:', witem.keys())
        print('found:', witem['title'], witem['score'])
        print(' ',witem.keys())
        n = n+1
        if n > 10:
            break

