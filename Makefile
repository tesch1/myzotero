init: ENV
	source ./ENV/bin/activate && python --version
	source ./ENV/bin/activate && pip install -r requirements.txt

ENV:
	python3 -m venv ENV

.PHONY: init
