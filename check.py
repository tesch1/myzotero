#!/usr/bin/env python
#
# get the data in a panda frame
#

from __future__ import print_function
from pyzotero import zotero
from crossref.restful import Works

# in config.py:
#
#    library_id='...'
#    library_type='user'
#    api_key='...'
#
library_type='user'
exec(open("./config.py").read())

if __name__ == "__main__":

    zot = zotero.Zotero(library_id, library_type, api_key)
    #items = zot.top(limit=5)
    items = zot.everything(zot.top())
    print("number of items: %d" % len(items))
    exit(1)
    # we've retrieved the latest five top-level items in our library
    # we can print each item's item type and ID
    if len(items):
        print('Item keys: %s' % items[0].keys())
        print('Data keys: %s' % items[0]['data'].keys())

    works = Works()
    for item in items:
        title = item['data']['title']
        author = item['data']['author']
        if 'DOI' in item['data']:
            doi = item['data']['DOI']
        else:
            doi = None
        
        if not doi or len(doi) == 0:
            print('Item Type: %s | Key: %s | DOI: %s | Title: %s' %
                  (item['data']['itemType'],
                   item['data']['key'],
                   doi,
                   title,
               ))
            f = {
                'title': title,
            }
            if author:
                f['author'] = author
            q = works.query(**f)
            matches = q.count()
            w1 = q.sort('relevance').order('asc')
            print('Url: %s | Number of matches: %d' % (q.url, matches))
            n = 0
            for witem in w1:
                #print('keys:', witem.keys())
                print('found:', witem['title'])
                n = n+1
                if n > 10:
                    break
            break

